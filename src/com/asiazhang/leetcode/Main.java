package com.asiazhang.leetcode;

import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here
        System.out.println(normalizePath("/home/"));
        System.out.println(normalizePath("/home//foo/"));
        System.out.println(normalizePath("/a/./b/../../c/"));
        System.out.println(normalizePath("/a/../../b/../c//.//"));
        System.out.println(normalizePath("/a//b////c/d//././/.."));
    }

    static public String normalizePath(String path) {
        String[] levels = path.split("/");

        Stack<String> paths = new Stack<>();
        for (String l : levels) {
            if (l.isEmpty()) {
                continue;
            }
            switch (l) {
                case ".":
                    // do nothing
                    break;
                case "..":
                    if (!paths.isEmpty()) {
                        paths.pop();
                    }

                    break;
                default:
                    paths.push(l);
                    break;
            }
        }


        return "/" + String.join("/", paths);

    }
}
