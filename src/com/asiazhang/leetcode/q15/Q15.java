package com.asiazhang.leetcode.q15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Q15 {

    static class Solution {
        public List<List<Integer>> threeSum(int[] nums) {
            if (nums.length < 3) {
                return new ArrayList<>();
            }
            List<List<Integer>> result = new ArrayList<>();
            Arrays.sort(nums);
            System.out.println(Arrays.toString(nums));
            int length = nums.length;
            for (int i = 0; i < length; i++) {
                if (nums[i] > 0) {
                    return result;
                }

                if (i > 0 && nums[i] == nums[i - 1]) {
                    continue;
                }

                int j = i+1;
                int k = length -1;

                while (j < k) {
                    int sum = nums[i] + nums[j] + nums[k];
                    if (sum == 0) {
                        result.add(Arrays.asList(nums[i], nums[j], nums[k]));
                        while (j < k && nums[j] == nums[j+1]) {
                            j++;
                        }
                        while (j < k && nums[k] == nums[k-1]) {
                            k--;
                        }
                        j++;
                        k--;
                    }
                    else if(sum > 0) {
                        k--;
                    }
                    else {
                        j++;
                    }
                }
            }

            return result;
        }
    }


    public static void main(String[] args) {
        Solution solution = new Solution();
        List<List<Integer>> r = solution.threeSum(new int[]{-1, 0, 1, 2, -1, -4,5,-3,-2,1,1,1,-4,-3,10,-8});
        System.out.println(r);
    }
}
