package com.asiazhang.leetcode.q1391;


public class Q1391 {

    static class Solution {
        static final int MAX = 3*3;

        static class DisjointSet {

            int[] results = new int[MAX];

            public DisjointSet() {
                for (int i = 0; i < MAX; i++) {
                    results[i] = i;
                }
            }

            public int find(int x) {
//                if (x == results[x]) {
//                    return x;
//                }
//
//                // 每一级都设置父子关系
//                results[x] = find(results[x]);
//                return results[x];

                int r = x;
                while (results[r] != r) {
                    r = results[r];
                }
                return r;
            }

            public void join(int x, int y) {
                // 联通x和y的上级节点
                int fx = find(x);
                int fy = find(y);
                if (fx != fy) {
                    results[fx] = fy;
                }
            }
        }

        DisjointSet ds = new DisjointSet();

        public boolean hasValidPath(int[][] grid) {
            int rows = grid.length;
            int columns = grid[0].length;

            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < columns; c++) {
                    switch (grid[r][c]) {
                        case 1:
                            checkLeft(grid, rows, columns, r, c);
                            checkRight(grid, rows, columns, r, c);
                            break;
                        case 2:
                            checkUp(grid, rows, columns, r, c);
                            checkDown(grid, rows, columns, r, c);
                            break;
                        case 3:
                            checkLeft(grid, rows, columns, r, c);
                            checkDown(grid, rows, columns, r, c);
                            break;
                        case 4:
                            checkRight(grid, rows, columns, r, c);
                            checkDown(grid, rows, columns, r, c);
                            break;
                        case 5:
                            checkUp(grid, rows, columns, r, c);
                            checkLeft(grid, rows, columns, r, c);
                            break;
                        case 6:
                            checkUp(grid, rows, columns, r, c);
                            checkRight(grid, rows, columns, r, c);
                            break;
                    }
                }
            }

            return ds.find(getId(rows, 0, 0)) == ds.find(getId(rows, columns - 1, rows - 1));
        }

        public int getId(int m, int x, int y) {
            return x * m + y;
        }

        public void checkLeft(int[][] grid, int rows, int columns, int r, int c) {
            if (c - 1 >= 0) {
                int street = grid[r][c - 1];
                if (street == 4 || street == 6 || street == 1) {
                    ds.join(getId(columns, r, c), getId(columns, r, c - 1));
                }
            }
        }

        public void checkRight(int[][] grid, int rows, int columns, int r, int c) {
            if (c + 1 < columns) {
                int street = grid[r][c + 1];
                if (street == 1 || street == 3) {
                    ds.join(getId(columns, r, c), getId(columns, r, c + 1));
                }
            }
        }

        public void checkUp(int[][] grid, int rows, int columns, int r, int c) {
            if (r - 1 >= 0) {
                int street = grid[r - 1][c];
                if (street == 2 || street == 3 || street == 4) {
                    ds.join(getId(columns, r, c), getId(columns, r - 1, c));
                }
            }
        }

        public void checkDown(int[][] grid, int rows, int columns, int r, int c) {
            if (r + 1 < rows) {
                int street = grid[r + 1][c];
                if (street == 2 || street == 5 || street == 6) {
                    ds.join(getId(columns, r, c), getId(columns, r + 1, c));
                }
            }
        }


    }

    public static void main(String[] args) {
        Solution solution = new Solution();
//        System.out.println(solution.hasValidPath(new int[][]{{1, 1, 2}}));
        System.out.println(solution.hasValidPath(new int[][]{{2, 4, 3}, {6, 5, 2}}));
    }
}
